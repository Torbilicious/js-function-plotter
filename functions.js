Functiontype = {
    SURFACE: "SURFACE",
    PARAMETRIC: "PARAMETRIC"
};

// Array of functions to plot and display inside the quiz
var Questions =
    [
        {
            answers: ["` sin(x * y) `", "` cos(x+y) `", "` 1/ln(x*y) `", "` 2x+5y `"],
            func: function (x, y) { return sin(x*y);},
            alreadyAsked: false,
            camera: {x: 3.5, y: 1.4, z: -2.3},
            type: Functiontype.SURFACE
        },
        {
            answers: ["` sqrt(x*y) `", "` cos(x+y) `", "` 1/ln(x*y) `", "` 2x+5y `"],
            func: function (x, y) { return Math.sqrt(x*y); },
            alreadyAsked: false,
            camera: null,
            type: Functiontype.SURFACE
        },
        {
            answers: ["` x^2 + 2.5y^2 -y * exp(1-(x^2+y^2))* 0.3 `", "` 3y `", "` 3z*4y `", "` cos(x + y * 5) `"],
            func: function (x, y) { return ((x*x) +2.5*(y*y)-y)*Math.exp(1-((x*x)+(y*y)))* 0.3; },
            alreadyAsked: false,
            camera: null
        },
        {
            answers: ["` 1 - (x^2 + y^2) `", "` cos(x+y) `", "` 1/ln(x*y) `", "` 2x+5y `"],
            func: function (x, y) { return 1 - (x*x+y*y); },
            alreadyAsked: false,
            camera: null,
            type: Functiontype.SURFACE
        },
        {
            answers: ["` sin(x+y)*cos(y+x) `", "` cos(x+y)*sin(x*y) `", "` sin(x*y)*cos(y*x) `", "` tan(x+y) `"],
            func: function (x, y) { return sin(x+y)*cos(y+x); },
            alreadyAsked: false,
            camera: null,
            type: Functiontype.SURFACE
        },
        {
            answers: ["` ln(1/x)*y `", "` cos(x+y)*sin(x*y) `", "` sin(x*y)*cos(y*x) `", "` tan(x+y) `"],
            func: function (x, y) { return Math.log(1/x)*y; },
            alreadyAsked: false,
            camera: null,
            type: Functiontype.SURFACE
        }
        // ,
        // {
        //     answers: ["` Antwort 1 `", "` Antwort 2 `", "` Antwort 3 `", "` Antwort 4 `"],
        //     func: function (t) {
        //         return {
        //             x: (11 * Math.cos(t) - 6 * Math.cos(11 / 6 * t)) / 4,
        //             y: (11 * Math.sin(t) - 6 * Math.sin(11 / 6 * t)) / 4,
        //             z: (Math.sin(t) - Math.cos(t)) * 5
        //         }
        //     },
        //     alreadyAsked: false,
        //     camera: {x: 1, y: 0.01, z: 1},
        //     type: Functiontype.PARAMETRIC
        // }
    ];